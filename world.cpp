#include <iostream>
#include <math.h>

#include "world.h"
#include "player.h"
#include "rect.h"

World::World(SDL_Renderer* _renderer) {
	renderer = _renderer;
	player = new Player(renderer, this);
	player->setSprite("sprites/player.png");

	economy = new Economy();

	WorldToScreenRatio.set(Vector2(cell_size, cell_size));
	camera_pos.set(Vector2(0, 0));
}

World::~World() {
	std::vector<Entity*>::const_iterator _e;
	for (_e=entities.begin(); _e!=entities.end(); ++_e) {
		if (_e != entities.end()) {
			(*_e)->~Entity();
		}
	}

	economy->~Economy();
	economy = NULL;

	player->~Player();
	player = NULL;

	renderer = NULL;

}

void World::setHud(HUD* _hud) {
	hud = _hud;
}

void World::render_sprite(SDL_Texture* _texture, Vector2* _pos) {
	Vector2 _screen_pos = worldspace_to_screenspace(*_pos);
	SDL_Rect _dst = {_screen_pos.x, _screen_pos.y, 64, 64};

	SDL_RenderCopy(renderer, _texture, NULL, &_dst);
}

void World::update() {
	std::vector<Entity*>::const_iterator _e;
	for (_e=entities.begin(); _e!=entities.end(); ++_e) {
		if (_e != entities.end()) {
			(*_e)->update();
		}
	}

	economy->update();
}

bool World::is_space_for_entity(Vector2 _pos) {
	bool space_is_available = true;
	Vector2 player_pos = *(player->getPos());
	Vector2 trunc_player_pos = Vector2(trunc(player_pos.x), trunc(player_pos.y));

	std::cout << player_pos.x << ", " << player_pos.y << "\n";
	std::cout << _pos.x << ", " << _pos.y << "\n";

	if (returnEntityByPos(_pos) == entities.end()) {
		int possible_x [2] = {trunc_player_pos.x, trunc_player_pos.x};
		int possible_y [2] = {trunc_player_pos.y, trunc_player_pos.y};

		if (fmod(player_pos.x, 1)) {
			// if the player is overlapping two squares on the X axis, include both
			if (possible_x[1] < 0) {
				possible_x[1]--;
			}
			else {
				possible_x[1]++;
			}
		}

		if (fmod(player_pos.y, 1)) {
			// if the player is overlapping two squares on the Y axis, include both
			if (possible_y[1] < 0) {
				possible_y[1]--;
			}
			else {
				possible_y[1]++;
			}
		}

		// if any of the players potential overlaps conflict with the proposed location, return false.
		for (int x=0; x<=1; x++) {
			for (int y=0; y<=1; y++) {
				Vector2 tmp_ply_pos = Vector2(possible_x[x], possible_y[y]);

				if (vector2_Equals(tmp_ply_pos, _pos)) {
					space_is_available = false;
				}
			}
		}
	}
	else {
		space_is_available = false;
	}

	return space_is_available;
}

void World::input(SDL_Event event) {
	if (event.type == SDL_MOUSEBUTTONDOWN) {

		// Placing entity
		if (event.button.button == SDL_BUTTON_LEFT) {
			Vector2 screen_pos(event.button.x, event.button.y);
			Vector2 world_pos = screenspace_to_worldspace_round(screen_pos);

			std::string _type = player->inventory->inventory[hud->returnInventorySlot()]->item_name;
			if (player->inventory->isSpace(_type)) {
				bool space_is_available = is_space_for_entity(world_pos);
				if (space_is_available) {
					if(player->inventory->removeItem(_type)) {
						insertEntity(new Entity(renderer, _type, world_pos, economy));
						std::cout << _type << " entity has been created\n";
					}
				}
				else {
					std::cout << world_pos.x << ", " << world_pos.y << " is occupied\n";
				}
			}
		}

		// Removing Entity
		else if (event.button.button == SDL_BUTTON_RIGHT) {
			Vector2 screen_pos(event.button.x, event.button.y);
			Vector2 world_pos = screenspace_to_worldspace_round(screen_pos);

			bool space_occupied = (returnEntityByPos(world_pos) != entities.end());
			std::string _type = (*returnEntityByPos(world_pos))->type;
			if (player->inventory->isSpace(_type)) {

				if (space_occupied) {
					removeEntity(world_pos);
					player->inventory->insertItem(_type);
				}
				else {
					std::cout << world_pos.x << ", " << world_pos.y << " is already empty\n";
				}
			}
		}
	}
	if (event.type == SDL_MOUSEWHEEL) {
		if (event.wheel.y > 0) {
			std::cout << "scroll up\n";
			hud->increaseInventorySlot();
		}
		else if (event.wheel.y < 0) {
			std::cout << "scroll down\n";
			hud->decreaseInventorySlot();
		}
	}
}

void World::render() {
	Vector2 camera_pos_pixel(worldspace_to_pixelspace(*player->getPos()));
	int winX = 1000;
	int winY = 600;
	int player_width = 64;

	Vector2 conversion = Vector2((winX-player_width)/2, (winY-player_width)/2);
	// centered camera = ((screen dimensions-player dims) / 2)
	Vector2 camera_pos_centered(vector2_Subtract(camera_pos_pixel, conversion));
	camera_pos.set(camera_pos_centered);

	// Render World
	for (int i=0; i<15; i++) {
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		if (i == 0) {
			SDL_SetRenderDrawColor(renderer, 255, 0,0,255);
		}
		// Vertical
		SDL_RenderDrawLine(renderer, ((i*cell_size) - camera_pos.x), 0, ((i*cell_size) - camera_pos.x), 1000);
		// Horizontal
		SDL_RenderDrawLine(renderer, 0, ((i*cell_size) - camera_pos.y), 1000, ((i*cell_size) - camera_pos.y));
		// grid[x][y]->render();
	}


	std::vector<Entity*>::const_iterator _e;
	for (_e=entities.begin(); _e!=entities.end(); ++_e) {
		if (_e != entities.end()) {
			render_sprite((*_e)->getTexture(), &(*_e)->pos);
		}
	}

	Vector2 player_pos_offset = Vector2(player->getPos()->x-0.05, player->getPos()->y-0.05);
	render_sprite(player->getTexture(), &player_pos_offset);
	// player->render();
}

Vector2 World::worldspace_to_pixelspace(Vector2 world_pos) {
	return vector2_Multiply(world_pos, WorldToScreenRatio);
}

Vector2 World::pixelspace_to_worldspace(Vector2 pixel_pos) {
	return vector2_Divide(pixel_pos, WorldToScreenRatio);
}

Vector2 World::worldspace_to_screenspace(Vector2 world_pos) {
	// screenspace = (world_pos * WorldToScreenRatio) + camera_pos
	return vector2_Subtract(vector2_Multiply(world_pos, WorldToScreenRatio), camera_pos);
}

Vector2 World::screenspace_to_worldspace(Vector2 screen_pos) {
	// worldspace = (screenspace - camera_pos) / WorldToScreenRatio
	return vector2_Divide(vector2_Add(screen_pos, camera_pos), WorldToScreenRatio );
}

Vector2 World::screenspace_to_worldspace_round(Vector2 screen_pos) {
	// worldspace = (screenspace - camera_pos) / WorldToScreenRatio
	return vector2_Divide_round(vector2_Add(screen_pos, camera_pos), WorldToScreenRatio );
}

bool World::insertEntity(Entity* _entity) {
	if (returnEntityByPos(_entity->pos) == entities.end()) {
		entities.push_back(_entity);
		return true;
	} else {
		return false;
	}
}

bool World::removeEntity(Vector2 _pos) {
	std::vector<Entity*>::const_iterator _entity = returnEntityByPos(_pos);
	if (_entity != entities.end()) {
		(*_entity)->~Entity();
		entities.erase(_entity);
	}
}

std::vector<Entity*>::const_iterator World::returnEntityByPos(Vector2 _pos) {
	bool is_occupied = false;
	std::vector<Entity*>::const_iterator _i;
	for (_i=entities.begin(); _i!=entities.end(); ++_i) {
		if (_i != entities.end()) {
			if (vector2_Equals((*_i)->pos, _pos)) {
				is_occupied = true;
				return _i;
			}
		}
	}
	return entities.end();
}

bool World::is_colliding(Vector2 _pos) {
	Rect b(_pos.x, _pos.y, 0.9, 0.9);

	std::vector<Entity*>::const_iterator _i;
	for (_i=entities.begin(); _i!=entities.end(); ++_i) {
		if (_i != entities.end()) {
			Rect a((*_i)->pos.x, (*_i)->pos.y, 1, 1);

			bool collision = (
				( a.x < ( b.x + b.w )
				&& ( a.x + a.w ) > b.x )
				&& ( a.y < ( b.y + b.h )
				&& ( a.y + a.h ) > b.y )
			);

			if (collision) {
				return true;
			}
		}
	}

	return false;
}
