#include "iostream"
#include "SDL2/SDL_image.h"
#include <nlohmann/json.hpp>

#include "craft_hud.h"
#include "type.h"


CraftHUD::CraftHUD(SDL_Renderer* _renderer, Player* _player) {
	visible = false;
	renderer = _renderer;
	player = _player;

	hudFont = TTF_OpenFont("RobotoSlab-Medium.ttf", 48);
	if (hudFont == NULL) {
		std::cout << TTF_GetError() << std::endl;
	}
}

CraftHUD::~CraftHUD() {
	renderer = NULL;
}

bool CraftHUD::isOverlap(int _x, int _y, SDL_Rect* target) {
	bool overlapping = false;
	int x = _x;
	int y = _y;

	x = x - target->x;
	y = y - target->y;

	if ( (x < target->w) && (y < target->h) ) {
		overlapping = true;
	}

	return overlapping;
}

bool CraftHUD::input(SDL_Event event) {
	bool handled = false;
	if (visible) {
		if (event.type == SDL_MOUSEBUTTONDOWN) {
			if (event.button.button == SDL_BUTTON_LEFT) {
				if (isOverlap(event.button.x, event.button.y, &background)) {
					handled = true;
					int count = 0; 

					bool done = false;
					for (auto item=types.begin(); item!=types.end(); ++item) {
						if (done) {
							break;
						}
						else if (!(*item)["recipe"].empty()) {
							SDL_Rect itemrect = { 210+(74*count), 210, 64, 64 };
							if (isOverlap(event.button.x, event.button.y, &itemrect)) {
								player->inventory->craft(item.key());
								done = true;
								break;
							}
							count++;
						}
					}
				}
			}
		}
	}
	
	// Toggle Inventory
	if (event.type == SDL_KEYDOWN) {
		if(event.key.keysym.sym == SDLK_e) {
			visible = !visible;
			handled = true;
		}
	}

	return handled;
}

void CraftHUD::render() {
	if (visible) {
		SDL_Texture* sprite = NULL;
		/*
		SDL_Color textColor = {255, 255, 255};

		std::string hud_text = "THIS IS TEXT";

		SDL_Surface* craftSurface = TTF_RenderText_Blended_Wrapped(hudFont, hud_text.c_str(), textColor, 400);

		SDL_Texture* hudTexture = SDL_CreateTextureFromSurface(renderer, craftSurface);

		int texW = 0;
		int texH = 0;
		SDL_QueryTexture(hudTexture, NULL, NULL, &texW, &texH);

		SDL_Rect dstrect;
		dstrect.x = 200;
		dstrect.y = 200;
		dstrect.w = texW/2;
		dstrect.h = texH/2;
		*/



		SDL_SetRenderDrawColor(renderer, 0x33, 0x33, 0x33, 0xFF);
		SDL_RenderFillRect(renderer, &background);

		//SDL_RenderCopy(renderer, hudTexture, NULL, &dstrect);
		int count = 0; 
		for (auto item=types.begin(); item!=types.end(); ++item) {
			if (!(*item)["recipe"].empty()) {
				std::string sprite_path = "sprites/" + item.key() + ".png";
				SDL_Texture* sprite = IMG_LoadTexture(renderer, sprite_path.c_str());
				if (sprite == NULL) {
					std::cout << "Failed to load sprite\n";
				}
				else {
					SDL_Rect itemrect = { 210+(74*count), 210, 64, 64 };
					SDL_RenderCopy(renderer, sprite, NULL, &itemrect);
					count++;
					//break;
				}

				SDL_DestroyTexture(sprite);
			}
		}


		//SDL_FreeSurface(craftSurface);
		//SDL_DestroyTexture(hudTexture);
		//craftSurface = NULL;
	}
}
