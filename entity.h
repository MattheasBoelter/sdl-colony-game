#ifndef ENTITY_H
#define ENTITY_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

#include "vector2.h"
#include "economy.h"

class Entity {
	public:
		Entity(SDL_Renderer* renderer, std::string _type, Vector2 _pos, Economy* _economy);
		~Entity();

		void update();

		void setPos(Vector2 _pos);
		void setSprite(std::string path);
		SDL_Texture* getTexture();
		Vector2 getPos();
		Vector2 pos;
		std::string type;

	private:
		SDL_Renderer* renderer;
		SDL_Texture* sprite;
		Economy* economy;
};

#endif //ENTITY_H
