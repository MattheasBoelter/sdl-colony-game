#ifndef WORLD_H
#define WORLD_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <iostream>
#include <string>
#include <vector>

#include "entity.h"
#include "vector2.h"
#include "player.h"
#include "economy.h"
#include "hud.h"

class Player;
class HUD;

class World {
	public:
		World(SDL_Renderer* _renderer);
		~World();

		void setHud(HUD* _hud);

		void update();
		void render();
		void input(SDL_Event event);

		bool insertEntity(Entity* entity);
		bool removeEntity(Vector2 _pos);
		std::vector<Entity*>::const_iterator returnEntityByPos(Vector2 _pos);
		std::vector<Entity*> entities;
		Player* player = NULL;

		Vector2 worldspace_to_pixelspace(Vector2);
		Vector2 pixelspace_to_worldspace(Vector2);
		Vector2 screenspace_to_worldspace(Vector2);
		Vector2 screenspace_to_worldspace_round(Vector2);
		Vector2 worldspace_to_screenspace(Vector2);

		int cell_size = 64;
		Vector2 camera_pos;
		Vector2 WorldToScreenRatio;

		bool is_colliding(Vector2);
	
	private:
		bool is_space_for_entity(Vector2 _pos);
		void render_sprite(SDL_Texture* _texture, Vector2* _pos);

		SDL_Renderer* renderer;
		Economy* economy;
		HUD* hud;
};

#endif // WORLD_H
