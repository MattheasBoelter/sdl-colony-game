#ifndef ECONOMY
#define ECONOMY

class Economy {
	public:
		Economy();
		~Economy();

		void update();

		void applyIncome(float income);
		bool applyExpense(float expense);

		float returnBalance();
	
	private:
		float balance = 0;
};

#endif // ECONOMY
