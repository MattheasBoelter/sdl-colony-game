OBJS = *.cpp
OBJ_NAME = colony

CC = g++
COMPILER_FLAGS = -w -g
LINKER_FLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf

all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
