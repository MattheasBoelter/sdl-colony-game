#include <cmath>
#include <iostream>
#include "vector2.h"

bool vector2_Equals(Vector2 a, Vector2 b) {
	return ((a.x == b.x) * (a.y == b.y));
}

Vector2 vector2_Multiply(Vector2 a, Vector2 b) {
	return Vector2((a.x * b.x), (a.y * b.y));
}

Vector2 vector2_Divide(Vector2 a, Vector2 b) {
	return Vector2((a.x / b.x), (a.y / b.y));
}

Vector2 vector2_Divide_round(Vector2 a, Vector2 b) {
	return Vector2(std::floor(a.x / b.x), std::floor(a.y / b.y));
}

Vector2 vector2_Add(Vector2 a, Vector2 b) {
	return Vector2((a.x + b.x), (a.y + b.y));
}

Vector2 vector2_Subtract(Vector2 a, Vector2 b) {
	return Vector2((a.x - b.x), (a.y - b.y));
}

Vector2::Vector2(float _x, float _y) {
	x = _x;
	y = _y;
}

Vector2::Vector2() {
	x = 0;
	y = 0;
}

void Vector2::set(Vector2 a) {
	x = a.x;
	y = a.y;
}
