#include <iostream>
#include <cmath>
#include "game.h"

Game::Game() {
	SDL_Init(0);
	TTF_Init();

	window = SDL_CreateWindow("Colony", 0, 0, 1000, 600, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_RenderSetLogicalSize(renderer, 1000, 600);

	running = true;

	world = new World(renderer);

	hud = new HUD(renderer, world->player);

	world->setHud(hud);

	loop();
}

Game::~Game() {
	world->~World();
	world = NULL;

	hud->~HUD();
	hud = NULL;

	SDL_DestroyRenderer(renderer);
	renderer = NULL;

	SDL_DestroyWindow(window);
	window = NULL;

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

void Game::loop() {
	while(running) {
		input();
		update();
		render();
	}
}

void Game::render() {
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);

	world->render();
	hud->render();

	SDL_RenderPresent(renderer);
}

void Game::input() {
	SDL_Event event;
	const Uint8 *key_state = SDL_GetKeyboardState(NULL);


	// Check if the game should close
	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_QUIT) {
			running = false;
		}
		if (!(hud->input(event))) {
			world->input(event);
		}
	}

	if (key_state[SDL_SCANCODE_ESCAPE]) {
		running = false;
	}
	//if (key_state[SDL_SCANCODE_Q]) {
	//	world->player->inventory->craft("wood_wall");
	//}

	// Player movement
	if (key_state[SDL_SCANCODE_W]) {
		world->player->move(Vector2(0, -0.05));
	}
	if (key_state[SDL_SCANCODE_S]) {
		world->player->move(Vector2(0, 0.05));
	}
	if (key_state[SDL_SCANCODE_A]) {
		world->player->move(Vector2(-0.05, 0));
	}
	if (key_state[SDL_SCANCODE_D]) {
		world->player->move(Vector2(0.05, 0));
	}

	world->player->applyVelocity();
}

void Game::update() {
	world->update();
}
