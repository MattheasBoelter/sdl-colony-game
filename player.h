#ifndef PLAYER_H
#define PLAYER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>

#include "vector2.h"
#include "world.h"
#include "inventory.h"

class World;

class Player {
	public:
		Player(SDL_Renderer* tmp_ren, World* _world);
		~Player();

		SDL_Texture* getTexture();
		void setSprite(std::string path);

		void move(Vector2);
		void applyVelocity();
		void discardVelocity();
		Vector2* getPos();
		Inventory* inventory;
	
	private:
		SDL_Renderer* renderer = NULL;
		SDL_Texture* sprite = NULL;
		World* world = NULL;

		Vector2 pos;
		Vector2 velocity;
};

#endif // PLAYER_H
