#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "player.h"
#include "entity.h"
#include "world.h"
#include "hud.h"

class Game {
	public:
		Game();
		~Game();
		void loop();

		void update();
		void render();
		void input();

	private:
		SDL_Renderer* renderer = NULL;
		SDL_Window* window = NULL;

		World* world = NULL;
		HUD* hud = NULL;

		bool running;
};

#endif //GAME_H
