#ifndef HUD_H
#define HUD_H

#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "player.h"
#include "craft_hud.h"

class Player;
class CraftHUD;

class HUD {
	public:
		HUD(SDL_Renderer* r, Player* p);
		~HUD();

		//void update();
		void render();
		bool input(SDL_Event event);

		void increaseInventorySlot();
		void decreaseInventorySlot();
		int returnInventorySlot();

	private:
		SDL_Renderer* renderer;
		SDL_Surface* hudSurface;
		TTF_Font* hudFont;
		SDL_Texture* hudTexture;
		SDL_Color textColor;

		Player* player = NULL;

		int inventory_slot;
		std::string hud_text;
		std::string active = "*";

		CraftHUD* craft_hud;
};

#endif //HUD_H
