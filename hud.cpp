#include "hud.h"

HUD::HUD(SDL_Renderer* _renderer, Player* _player) {
	player = _player;
	renderer = _renderer;

	textColor = {255, 255, 255};

	hudFont = TTF_OpenFont("RobotoSlab-Medium.ttf", 48);
	if (hudFont == NULL) {
		std::cout << TTF_GetError() << std::endl;
	}

	craft_hud = new CraftHUD(renderer, player);

	hudTexture = NULL;

	inventory_slot = 0;

	hud_text = "";
}

HUD::~HUD() {
	//Free what we initiated
	SDL_FreeSurface(hudSurface);
	hudSurface = NULL;

	SDL_DestroyTexture(hudTexture);
	hudTexture = NULL;

	TTF_CloseFont(hudFont);
	hudFont = NULL;

	craft_hud->~CraftHUD();
	craft_hud = NULL;

	//NULL pointers to what we didn't initiate
	player = NULL;
	renderer = NULL;
}

bool HUD::input(SDL_Event event) {
	return craft_hud->input(event);
};

void HUD::render() {
	std::string sprite_path = "";

	for (int i=0; i<player->inventory->inventory_size; i++) {
		SDL_Rect itemrect = { 210+(74*i), 500, 64, 64 };

		if (i == inventory_slot) {
			SDL_Rect active_rect = { itemrect.x-5, itemrect.y-5, 74, 74 };
			SDL_SetRenderDrawColor(renderer, 0x55, 0x55, 0x55, 0xFF);
			SDL_RenderFillRect(renderer, &active_rect);
		}

		if (player->inventory->inventory[i] != NULL) {
			std::string sprite_path = "sprites/" + player->inventory->inventory[i]->item_name + ".png";
			SDL_Texture* sprite = IMG_LoadTexture(renderer, sprite_path.c_str());
			if (sprite == NULL) {
				std::cout << "Failed to load sprite\n";
			}
			else {
				std::string count_text = std::to_string(player->inventory->inventory[i]->returnItemCount());
				SDL_Surface* count_surface = TTF_RenderText_Blended(hudFont, count_text.c_str(), textColor);
				SDL_Texture* count_texture = SDL_CreateTextureFromSurface(renderer, count_surface);

				int texW = 0;
				int texH = 0;
				SDL_QueryTexture(count_texture, NULL, NULL, &texW, &texH);
				SDL_Rect count_rect = { itemrect.x+20, itemrect.y+20, texW/2, texH/2 };


				SDL_RenderCopy(renderer, sprite, NULL, &itemrect);
				SDL_RenderCopy(renderer, count_texture, NULL, &count_rect);

				SDL_FreeSurface(count_surface);
				SDL_DestroyTexture(count_texture);
				count_texture = NULL;
			}
			SDL_DestroyTexture(sprite);
		}
	}

	craft_hud->render();
}

void HUD::increaseInventorySlot() {
	inventory_slot++;

	if (inventory_slot == 10) {
		inventory_slot = 0;
	}
}

void HUD::decreaseInventorySlot() {
	if (inventory_slot == 0) {
		inventory_slot = 9;
	}
	else {
		inventory_slot--;
	}
}

int HUD::returnInventorySlot() {
	return inventory_slot;
}
