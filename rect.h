#ifndef RECT
#define RECT

class Rect {
	public:
		Rect(float _x, float _y, float _w, float _h);
		float x;
		float y;
		float w;
		float h;
};

#endif //RECT
