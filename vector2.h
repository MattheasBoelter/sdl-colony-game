#ifndef VECTOR2_H
#define VECTOR2_H

class Vector2 {
	public:
		Vector2();
		Vector2(float _x, float _y);
		float x;
		float y;
		void set(Vector2 a);
};

bool vector2_Equals(Vector2 a, Vector2 b);
Vector2 vector2_Add(Vector2 a, Vector2 b);
Vector2 vector2_Subtract(Vector2 a, Vector2 b);
Vector2 vector2_Multiply(Vector2 a, Vector2 b);
Vector2 vector2_Divide(Vector2 a, Vector2 b);
Vector2 vector2_Divide_round(Vector2 a, Vector2 b);

#endif // VECTOR2_H
