#ifndef INVENTORY_H
#define INVENTORY_H

#include <iostream>
#include <string>

class InventorySquare {
	public:
		InventorySquare(std::string _item, int _count, int _stack_size);
		~InventorySquare();

		int returnItemCount();
		bool addEntity();
		bool removeEntity();

		std::string item_name;
		int max_stack_size;

	private:
		int item_count;
};

class Inventory {
	public:
		Inventory();
		~Inventory();

		bool isSpace(std::string _item_name);
		bool insertItem(std::string _item_name);
		bool removeItem(std::string _item_name);
		void craft(std::string _item_name);

		InventorySquare* inventory[10];
		int inventory_size;

	private:
};


#endif //INVENTORY_H
