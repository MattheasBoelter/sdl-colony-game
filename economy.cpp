#include "economy.h"

#include <iostream>

Economy::Economy() {};

Economy::~Economy() {};

void Economy::update() {
	//std::cout << "Current balance: " << balance << "\n";
}

bool Economy::applyExpense(float expense) {
	if (expense <= balance) {
		balance = balance - expense;
		return true;
	}
	else {
		return false;
	}
}

void Economy::applyIncome(float income) {
	balance = balance + income;
}

float Economy::returnBalance() {
	return balance;
}
