#include "entity.h"
#include <iostream>

Entity::Entity(SDL_Renderer* _renderer, std::string _type, Vector2 _pos, Economy* _economy) {
	renderer = _renderer;
	economy = _economy;
	std::string _img_path = "sprites/";
	type = _type;
	setSprite(_img_path + type + ".png");

	setPos(_pos);
}

void Entity::update() {
	economy->applyIncome(0.1);
}

Entity::~Entity() {
	SDL_DestroyTexture(sprite);
	sprite = NULL;
	renderer = NULL;
}

SDL_Texture* Entity::getTexture() {
	return sprite;
}

void Entity::setPos(Vector2 _pos) {
	pos.x = _pos.x;
	pos.y = _pos.y;
}

void Entity::setSprite(std::string path) {
	sprite = IMG_LoadTexture(renderer, path.c_str());
	if (sprite == NULL) {
		std::cout << "Failed to load sprite\n";
	}
}
