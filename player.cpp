#include "player.h"
#include <iostream>

#include "world.h"
#include "vector2.h"

Player::Player(SDL_Renderer* tmp_ren, World* _world) {
	world = _world;
	renderer = tmp_ren;
	pos.set(Vector2(1, 1));
	velocity.set(Vector2(0, 0));

	inventory = new Inventory();

	for(int i=0; i<10; i++) {
		inventory->insertItem("wood");
		inventory->insertItem("stone");
	}
}

Player::~Player() {
	SDL_DestroyTexture(sprite);
	sprite = NULL;
	world = NULL;
	renderer = NULL;

	inventory->~Inventory();
	inventory = NULL;
}

void Player::setSprite(std::string path) {
	sprite = IMG_LoadTexture(renderer, path.c_str());
	if (sprite == NULL) {
		std::cout << "Failed to load sprite\n";
	}
}

SDL_Texture* Player::getTexture() {
	return sprite;
}

Vector2* Player::getPos() {
	return &pos;
}

void Player::move(Vector2 _vel) {
	Vector2 _prev_velocity(velocity);
	velocity.set(vector2_Add(velocity, _vel));

	if (world->is_colliding(vector2_Add(pos, velocity))) {
		velocity.set(_prev_velocity);
	}
}

void Player::discardVelocity() {
	velocity.set(Vector2(0, 0));
}

void Player::applyVelocity() {
	pos.set(vector2_Add(pos, velocity));

	velocity.set(Vector2(0, 0));
}
