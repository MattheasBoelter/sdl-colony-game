/* Inventory */
#include <iostream>
#include <nlohmann/json.hpp>

#include "inventory.h"
#include "type.h"

Inventory::Inventory() {
	inventory_size = 10;

	for (int i=0; i<inventory_size; i++) {
		inventory[i] = NULL;
	}
}

Inventory::~Inventory() {
	for (int i=0; i<inventory_size; i++) {
		if (inventory[i] != NULL) {
			inventory[i]->~InventorySquare();
			inventory[i] = NULL;
		}
	}
}

bool Inventory::insertItem(std::string _item_name) {
	std::cout << "Inventory::insertItem();\n";
	for (int i=0; i<inventory_size; i++) {
		if (inventory [i] != NULL) {
			std::cout << inventory[i]->item_name << ": "
				<< std::to_string(inventory[i]->returnItemCount()) << std::endl;

			if (_item_name == inventory[i]->item_name) {
				std::cout << inventory[i]->item_name << "\n";
				if (inventory[i]->returnItemCount() < inventory[i]->max_stack_size) {
					return inventory[i]->addEntity();
				}
			}
		}
		else {
			inventory[i] = new InventorySquare(_item_name, 1, 10);
			std::cout << inventory[i]->item_name << "\n";
			return true;
		}
	}

	return false;
}

bool Inventory::removeItem(std::string _item_name) {
	std::cout << "Inventory::removeItem();\n";

	for (int i=0; i<inventory_size; i++) {
		if (inventory[i] != NULL) {
			std::cout << inventory[i]->item_name << ": "
				<< std::to_string(inventory[i]->returnItemCount()) << std::endl;
			if (_item_name == inventory[i]->item_name) {
				if (inventory[i]->returnItemCount()) {
					return inventory[i]->removeEntity();
				}
			}
		}
		else {
			std::cout << "item not in inventory\n";
		}
	}

	return false;
}

bool Inventory::isSpace(std::string _item_name) {
	bool space_found = false;

	for(int i=0; i<10; i++) {
		if (inventory[i] != NULL) {
			if (inventory[i]->item_name == _item_name) {
				if (inventory[i]->returnItemCount() < inventory[i]->max_stack_size) {
					space_found = true;
					break;
				}
			}
		}
	}
	if (space_found == false) {
		for (int i=0; i<10; i++) {
			if (inventory[i] == NULL) {
				space_found = true;
				break;
			}
		}
	}

	return space_found;
}

void Inventory::craft(std::string _item_name) {
	nlohmann::json recipe = types[_item_name]["recipe"];
	bool item_added = true;
	for (auto item=recipe.begin(); item!=recipe.end(); ++item) {
		bool item_has_space = false;
		for (int i=0; i<10; i++) {
			if (inventory[i] != NULL) {
				if (inventory[i]->item_name == item.key()) {
					if (inventory[i]->returnItemCount() >= item.value()) {
						for (int c=0; c<item.value(); c++) {
							inventory[i]->removeEntity();
							item_has_space = true;
						}
					}
				}
			}
		}
		if (!item_has_space) {
			item_added = false;
		}
	}

	if (item_added) {
		insertItem(_item_name);
	}
}

/* InventorySquare */

InventorySquare::InventorySquare(std::string _item, int _count, int _stack_size) {
	item_name = _item;
	item_count = _count;
	max_stack_size = _stack_size;
}

InventorySquare::~InventorySquare() {
	//
}

int InventorySquare::returnItemCount() {
	return item_count;
}

bool InventorySquare::addEntity() {
	if (item_count + 1 <= max_stack_size) {
		item_count++;
		return true;
	}
	else {
		return false;
	}
}

bool InventorySquare::removeEntity() {
	if (item_count - 1 <= max_stack_size) {
		item_count--;
		return true;
	}
	else {
		return false;
	}
}

