#ifndef CRAFT_HUD_H
#define CRAFT_HUD_H

#include <string>
#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"

#include "player.h"

class Player;

class CraftHUD {
	public:
		CraftHUD(SDL_Renderer* _renderer, Player* _player);
		~CraftHUD();

		bool input(SDL_Event _event);
		void render();

		bool isOverlap(int _x, int _y, SDL_Rect* target);

	private:
		bool visible;
		std::string text;

		Player* player = NULL;
		SDL_Rect background { 200, 200, 500, 200 };
		SDL_Rect craftButton { 210, 210, 64, 64 };

		SDL_Renderer* renderer;
		TTF_Font* hudFont;
};

#endif //CRAFT_HUD_H
