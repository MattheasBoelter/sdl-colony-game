#ifndef TYPE_H
#define TYPE_H

#include <nlohmann/json.hpp>

inline nlohmann::json types = {
	{"wood", {
		{"name", "wood"},
		{"recipe", {
		}}
	}},
	{"stone", {
		{"name", "stone"},
		{"recipe", {
		}},
	}},
	{"wood_wall", {
		{"name", "wood_wall"},
		{"recipe", {
			{"wood", 2},
		}},
	}},
	{"stone_wall", {
		{"name", "stone_wall"},
		{"recipe", {
			{"stone", 2},
		}},
	}},
	{"wood_stone", {
		{"name", "wood_stone"},
		{"recipe", {
			{"stone", 1},
			{"wood", 1},
		}},
	}},
};

#endif //TYPE_H
